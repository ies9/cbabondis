# %%
import urllib.request
import time
import datetime
import pandas as pd
import json
import numpy as np

# %%

# esta función devuleve un diccionario vacío con la estructura de los dataframe a crear
def diccionario_inicial():
    return {
        'id': [],
        'latitud': [],
        'longitud': [],
        "nombre": [],
        "linea": [],
        'marca_tiempo': [],
        "ultima_posicion_time": [],
        "recorrido_actual": [],
        "ultimas_velocidades_promedio": [],
        "ultimas_variaciones_latitud": [],
        "ultimas_variaciones_longitud": [],
        "adaptado": [],
        "piso_bajo": [],
        "articulado": [],
        "empresa": [],
    }


# %%
datos_para_df = diccionario_inicial()

df = pd.DataFrame(datos_para_df)
df

# %%
# Cantidad de segundos para una semana de registros
segundos = 60*60*24*7
# Cantidad de segundos para un día de registros
# segundos = 60*60*24
# segundos = 30
desplazamientoUTC = -datetime.timedelta(hours=3)
tz = datetime.timezone(desplazamientoUTC)
dia = datetime.datetime.now(tz=tz).date().isoformat()

# bandera para repetir la consulta cuando falla
segundo_intento = False

for i in range(segundos):
    # a urllib.request.urlopen le paso la url donde se encuentra mi json y lo nombro url
    pagina = 'https://cordobus.apps.cordoba.gob.ar/tracking/api/internos-activos-ahora/?format=json'
    data_json = None
    datos_para_df = diccionario_inicial()
    marca_tiempo = datetime.datetime.now()

    while pagina is not None:
        # print(datetime.datetime.now().isoformat(), pagina)

        try:
            with urllib.request.urlopen(pagina) as url:
                data_json = json.load(url)
                segundo_intento = False
        except:
            print('Error al obtener los datos de: ', url)
            if segundo_intento: 
                segundo_intento = False
                break
            else:
                segundo_intento=True
                continue

        # próxima página (None si llega a la última)
        pagina = data_json['next']
        print(marca_tiempo.isoformat(), 'colectivos: ', data_json['count'])

        for colectivo in data_json['results']['features']:
            datos_para_df['id'].append(colectivo['id'])
            datos_para_df['longitud'].append(colectivo['geometry']['coordinates'][0])
            datos_para_df['latitud'].append(colectivo['geometry']['coordinates'][1])
            datos_para_df['nombre'].append(colectivo['properties']['nombre'])
            datos_para_df['linea'].append(colectivo['properties']['linea'])
            datos_para_df['marca_tiempo'].append(marca_tiempo)
            datos_para_df['ultima_posicion_time'].append(colectivo['properties']['ultima_posicion_time'])
            datos_para_df['recorrido_actual'].append(colectivo['properties']['recorrido_actual'])
            datos_para_df['ultimas_velocidades_promedio'].append(colectivo['properties']['ultimas_velocidades_promedio'])
            datos_para_df['ultimas_variaciones_latitud'].append(colectivo['properties']['ultimas_variaciones_latitud'])
            datos_para_df['ultimas_variaciones_longitud'].append(colectivo['properties']['ultimas_variaciones_longitud'])
            datos_para_df['adaptado'].append(colectivo['properties']['adaptado'])
            datos_para_df['piso_bajo'].append(colectivo['properties']['piso_bajo'])
            datos_para_df['articulado'].append(colectivo['properties']['articulado'])
            datos_para_df['empresa'].append(colectivo['properties']['empresa'])

    # Transformación del diccionario a un dataframe
    df_tmp = pd.DataFrame(datos_para_df)

    # Se eliminan los registros duplicados
    df = pd.concat([df, df_tmp]).drop_duplicates(
        ['id', 'ultima_posicion_time'])

    # Reescritura del archivo csv
    df.to_csv('../Base de Datos/cbabondis-'+dia+'.csv', index=False)

    # Pausa de un segundo para la próxima consulta
    time.sleep(1)

    # Para evitar perdida de datos se crea un nuevo csv cada día
    dia_tmp = datetime.datetime.now(tz=tz).date().isoformat()

    if dia_tmp != dia:
        dia = dia_tmp
        df = pd.DataFrame(diccionario_inicial())
